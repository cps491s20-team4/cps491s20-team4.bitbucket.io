University of Dayton

Department of Computer Science

CPS 491 - Spring 2020

Dr. Phu Phung


## Capstone II Proposal - Team 4


## Pluggable Web Search for Synchrony


# Team members



1.  Alex Smith, smitha86@udayton.edu
2.  Hao Wang, hwang7@udayton.edu


# Company Mentors

Joydeep Mukherjee, Joydeep.Mukherjee@syf.com, Ph.D., VP
Arpan Bhattacharya, Arpan.bhattacharya2@syf.com, Sr. Software Engineer

Synchrony

950 Forrer Boulevard

## Project Management Information
- Management Board: https://trello.com/b/XLaksU5S/cps491s20-team-4.
- Project homepage: https://pluggable-web-search.herokuapp.com/.
- Bitbucket Repository: https://cps491s20-team4.bitbucket.io/.
# Revision History

## Overview
Provide a fast and accurate web search plug-in that can be used by the Synchrony company and its partners to allow users to search connected web pages from a single search box.


## Project Context and Scope

The project will be able to be included in any of the company web sites via the inclusion of a javascript <script> tag. After that, whenever a user visits the web site, our code will create a search box that the user can use to search connected web sites, which will be crawled, indexed, and returned as search results to the user.

## System Analysis 

# High-level Requirements

- Crawl Connected Web Sites
- Index and Search Results
- Predict Search Queries, Premptively search based on frequent searches
- Provide Results Quickly
- Be able to be plugged into any web site

# Use cases

- Search uses case: user can use our plug-in to search keywords from websites
![Search uses case ](pngs/search.png "Search uses case")

- implement on website: allow user to use our plug-in on websites
![User implement ](pngs/UserImplement.png "User implement")

# System Design
![System Design](https://i.imgur.com/O1ZU7Ka.png "System Design")

## Use-Case Realization
1. Search use-case: it's a dedicated function of accepting user input to be searched for in a database.
2. implement on websites: user can add our search box into their websites, and use it to searh the key words

## Database information
We will be using firebase now, but we might be change later.

## User Interface
Once the external JavaScript has been included in a web page, a search box will appear when the page is loaded.

# Implementation
Implemented in three parts. The first, JavaScript that is included in the web page and adds the user interface. Second, the Node.js based web application that the user interface sends the search queries to, and that manages the database, crawling connected web sites, storing and indexing the results, and returning them to the user. Third, the database which stores the contents of web pages, where they can be searched by the user.

## Deployment
Our JavaScript code can be included in any web page using a <script> tag.


## Technology
Node.js, Socket.io web application, Firebase database


# Project Management

3 sprint cycles, meet several times a week. Before next semester we will learn about technologies, implement basic idea. 
- First cycle we will plan on implementing architechure, database and framework.
- Second Cycle we will develop web crawler, search functionality.
- Third Cycle we will test and finalize project, implement premptive search, optimize.
- Finally, we will present results.


## Scrum process

## Sprint 0
Duration: 01/13/2020 - 01/21/2020

Research technical requirements, prepare prototype.

## Sprint 1
Duration: 01/23/2020 - 02/11/2020

Finish prototype, create server-side application.

## Sprint 2
Duration: 02/13/2020 - 03/03/2020

Research Firebase alternatives and implement one of them, deploy server application on heroku.

## Sprint 3
Duration: 03/05/2020 - 03/31/2020

Optimize searching, prepare common search results on page load.

## Sprint 4
Duration 04/02/2020 - 04/23/2020

Learn user search trends, load relavent search results first, on page load.

### Complete Tasks
    1. Research technology requirements
    2. User interface prototype
    3. Server-side developed, deployed to heroku
    4. database implementation.

### Contributions:
    1. Member Alex Smith, 69 commits, 25 hours, contributed in Implemented database, improved pluggability, Trello board.
    2. Member Hao Wang, 13 commits, 20 hours, contributed in research, UI, interface testing, prepare for relative searching. Readme update, trello board.

### Sprint Retrospection

###   Good                Could have been better              How to Improve?

| Good               |   Could have been better    |  How to improve?                                                                 |
|--------------------|:---------------------------:|-------------------------------------------------------------------------------:  |
|Finished interface prototype, Researched database  | Need a better design,   Look more at previous project for what we want to use|    Expand search prototype, Implement database  |




# User guide/Demo
User guide and demo can be found at  https://cps491s20-team4.bitbucket.io/
![Demo ](pngs/demo1.png "Demo")

# Company Support

Contact available for leading us in the right derection, clear up requirements.