const bcrypt = require('bcryptjs');
const fireClient = require('firedb').fireClient;
const uri = "firebase+srv://user1:Pass1@cluster0-3sujq.mongodb.net/messenger?retryWrites=true&w=majority";
const firedbclient = new fireClient(uri, { useNewUrlParser: true, useUnifiedTopology: true });
let searcherdb = null;
const dbIsReady = ()=>{
    return searcherdb != null;
};
const connectDb = (ready) =>{
    if(dbIsReady())
        ready(null);
    else
        firedbclient.connect( (err,connection) => {
            if(err) ready(err);
            console.log("DB is connected!");
            searcherdb = connection.db();
            ready(null);
        });
};
const getDb = () =>{
    if(!dbIsReady())
        throw Error("No database connection");
    return searcherdb;
}