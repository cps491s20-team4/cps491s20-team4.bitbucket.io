    var autoNode1=$("#auto_obj") ;//way to load
    var highlightindex = -1;
    var timeout;
    var wordInput1 = $("#cl_obj");//search inputbox
    var card_id = "";
    
    var relativedb = require("./relativedb");
    relativedb.connectDb((err)=>{
    if(!err && relativedb.dbIsReady())
    console.log("relativedb is ready");
    else console.log("relativedb connection error:",err);
});
    wordInput1.keyup(function (event)//press keyboard
    {
        nameInput(wordInput1, autoNode1);
    });

    function nameInput(word_obj, obj_node) {
        var textInput1 = word_obj.val();//value of input
        var myEvent = event || window.event;//different event for browsers
        var keyCode = myEvent.keyCode;
        var outDivHeight = obj_node.height()-20; //
        if (word_obj.val() != "") 
        {
            if (keyCode == 38 || keyCode == 40) { 
                var autoNode1s = obj_node.children();
                if (keyCode == 38)
                {
                    if (highlightindex != -1)//
                    {
                        if(highlightindex==0){
 
                        }else{
                            word_obj.val($(this).attr("description"));
                            autoNode1s.eq(highlightindex).css("background-color", "white");
                            highlightindex--; 
 
                            var innerHeight = autoNode1s.eq(highlightindex).position().top||0;
                            if(innerHeight < 35){
                                var scrollheight = obj_node.scrollTop();
                                obj_node.scrollTop(scrollheight-200);
                            }
                        }
                    }
                    if (highlightindex == -1) { 
                        highlightindex = autoNode1s.length - 1;
                    }
                    autoNode1s.eq(highlightindex).css("background-color", "#ACD6FF");
                    word_obj.val(autoNode1s.eq(highlightindex).attr("description"));
                    
               
       $("#currentdeptcode").val(autoNode1s.eq(highlightindex).attr("dept_code"));
                } else if (keyCode == 40)
                {
                    if (highlightindex != -1)
                    {
                        if(autoNode1s.length == (highlightindex+1)){
 
                        }else{
                            autoNode1s.eq(highlightindex).css("background-color", "white");
                            highlightindex++; 
 
                            var innerHeight = autoNode1s.eq(highlightindex).position().top||0;
                            if(innerHeight >= outDivHeight){
                                var scrollheight = obj_node.scrollTop();
                                obj_node.scrollTop(scrollheight+200);
                            }
                        }
                    }
 
                    if (highlightindex == -1)
                    {
                        highlightindex++;
                    }
 
                    autoNode1s.eq(highlightindex).css("background-color", "#ACD6FF");
                    word_obj.val(autoNode1s.eq(highlightindex).attr("description"));
                    $("#currentdeptcode").val(autoNode1s.eq(highlightindex).attr("dept_code"));
                }
 
            } else if (keyCode == 13)//enter key
            {
                obj_node.hide();
                highlightindex = -1;
            } else {
                clearTimeout(timeout);
                if (keyCode != 13) {
                    timeout = setTimeout(function () {
                        getDateName(textInput1, obj_node, word_obj);
                    }, 500);
                }
            }
 
        } else {
            obj_node.hide();
            highlightindex = -1;
        }
    }
 
 
    function getDateName(txtInput, obj_node, word_obj) {
        $.post( "get_organdata", {"input": txtInput}, function (data) {
            var searchs = JSON.parse(data);
            obj_node.html("");
            if(searchs.length==0){
                $("#currentdeptcode").val('');
            }
            if (searchs.length > 0) {
                for (var i = 0; i < searchs.length; i++) {
                    var NewNode = $("<div>").attr("id", searchs[i].description);
                    var NewNode = $("<div>").attr("dept_code", searchs[i].seq_no);
                    NewNode.attr("description", searchs[i].description);
                    NewNode.html(searchs[i].description).appendTo(obj_node);
 
                    NewNode.hover(
                        function () {
                            if (highlightindex != -1) {
                                obj_node.children().eq(highlightindex).css("background-color", "white");
                            }
                            word_obj.val($(this).attr("description"));
                            $("#currentdeptcode").val($(this).attr("dept_code"));
                            highlightindex = $(this).attr("id");
                            $(this).css("background-color", "#ACD6FF");
                        },
                        function () {
                            $(this).css("background-color", "white");
                        }
                    );
                    NewNode.click(function () {
                        word_obj.val($(this).attr("description"));
 
                        $("#currentdeptcode").val($(this).attr("dept_code"));
                        var curDeptcode=$(this).attr("dept_code");
                        var orDeptcode='';
                        $.post("getCurrentDeptCode",function (rs) {
                            orDeptcode=rs;
                            if(curDeptcode==orDeptcode){
                                layer.msg('repeat name, please re-enter', {
                                    time: 2500,
                                });
                                $("#cl_obj").val("").focus();
                            }
                        },"text");
 
                        obj_node.hide();
                        highlightindex = -1;
                    });
                }
            }
            if (searchs.length ) 
            {
                obj_node.show();
            }
            else {
                obj_node.hide();
            }
        });
    }