var http = require("http"), fs = require('fs');
var server = http.createServer(httphandler);
var port = process.env.PORT || 8080;
server.listen(port);
function httphandler(request,response){
	// response.writeHead(200); // 200 OK 
  	// var clientUI_stream = fs.createReadStream('./index.html');
  	// clientUI_stream.pipe(response);
    // if(request.url.indexOf('.html') != -1){
      
    // }
    if(request.url.indexOf('.js') != -1){
      fs.readFile('./test.js', function (err, data) {
        if (err) console.log(err);
        response.writeHead(200, {'Content-Type': 'text/javascript'});
        response.write(data);
        response.end();
      });
    } else if(request.url.indexOf('.css') != -1){
      fs.readFile('./test.css', function (err, data) {
        if (err) console.log(err);
        response.writeHead(200, {'Content-Type': 'text/css'});
        response.write(data);
        response.end();
      });
    } else {
        fs.readFile('./index.html', function (err, data) {
        if (err) console.log(err);
        response.writeHead(200, {'Content-Type': 'text/html'});
        response.write(data);
        response.end();
        });
    }
}
console.log("ChatServer is running at port: " + port);
var io = require('socket.io');
var socketio = io.listen(server);
console.log("Socket.IO listening at port: " + port);
socketio.on("connection", function (socketclient) {
    console.log("client connected");
    socketclient.on("echo", function (search) {
        console.log("echoing back: " + search);
        socketclient.emit("echo", search);
    });
});